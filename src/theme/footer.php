    <!-- ****** Footer Area Start ****** -->
    <footer class="caviar-footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer-text">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Desarrollado </i> por <a href="https://dgk.com.mx" target="_blank">dgk</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ****** Footer Area End ****** -->

    <!-- Jquery-2.2.4 js -->
    <!-- Popper js -->
    <!-- Bootstrap-4 js -->
    <!-- All Plugins js -->
    <!-- Active JS -->

    <!-- All now in bundle -->
	<?php wp_footer(); ?>
</body>