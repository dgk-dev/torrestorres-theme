<?php

// INCLUDES
//resources: CSS JS FONTS
require_once( trailingslashit( get_template_directory() ). '/includes/resources.php' );

//Custom post type: facebook posts
require_once( trailingslashit( get_template_directory() ). 'includes/custom-post-type-fpost.php' );

//cleanup wordpress head
require_once( trailingslashit( get_template_directory() ). 'includes/cleanup.php' );

//Crear páginas necesarias al activar el theme
add_action("after_switch_theme", "torrestorres_create_pages");


function torrestorres_create_pages(){
    if (isset($_GET['activated']) && is_admin()){
        $pages = array(
            array(
                'title' => 'Home',
                'slug' => 'home'
            ),
            array(
                'title' => 'Quiénes somos',
                'slug' => 'quienes-somos'
            ),
            array(
                'title' => 'Auditoría',
                'slug' => 'auditoria'
            ),
            array(
                'title' => 'Asesoría y consultoría',
                'slug' => 'asesoria-consultoria'
            ),
            array(
                'title' => 'Impuestos y servicios legales',
                'slug' => 'impuestos-servicios-legales'
            ),
            array(
                'title' => 'Post',
                'slug' => 'post'
            ),
            array(
                'title' => 'Contacto',
                'slug' => 'contacto'
            )
        );

        foreach($pages as $page){
            $page_check = get_page_by_path($page['slug']);
            
            if(!isset($page_check->ID)){
                $page_title = $page['title'];
                $page_slug = $page['slug'];
                $page_content = '';
                $page_template = '';
                
                $new_page = array(
                    'post_type' => 'page',
                    'post_title' => $page_title,
                    'post_name' => $page_slug,
                    'post_content' => $page_content,
                    'post_status' => 'publish',
                    'post_author' => 1,
                );

                $new_page_id = wp_insert_post($new_page);
                
                if(!empty($page_template)){
                    update_post_meta($new_page_id, '_wp_page_template', $page_template);
                }
            }
        }

        $homepage = get_page_by_path('home');

        if (isset($homepage->ID)){
            update_option( 'page_on_front', $homepage->ID );
            update_option( 'show_on_front', 'page' );
        }
    }
}

show_admin_bar( false );

add_action( 'admin_menu', 'torrestorres_rename_labels' );

function torrestorres_rename_labels() {
    global $menu;
    $searchPlugin = "contact-form-listing";
    $replaceName = "Contactos de formulario";
    $replaceIcon = "dashicons-email";

    $menuItem = "";
    foreach($menu as $key => $item){
        if ( $item[2] === $searchPlugin ){
            $menuItem = $key;
        }
    }
    if($menuItem){
        $menu[$menuItem][0] = $replaceName;
        $menu[$menuItem][6] = $replaceIcon;
    }
}



add_action('wp_dashboard_setup', 'torrestorres_dashboard_widgets');

function torrestorres_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Bienvenido', 'custom_dashboard_message');
}
 
function custom_dashboard_message() {
    echo '<div style="padding:20px 0; background-color: #212121; text-align:center;"><img src="'.get_template_directory_uri().'/img/logo/lg-torres-148×50.png"></div><p><strong>Este es el administrador del sitio Torres Torres</strong></p><p>Desde aquí podrás gestionar los posts de facebook así como consultar y exportar la información de los usuarios que llenaron el formulario de contacto<p><p>En la sección <strong>Perfil</strong> podrás editar tus datos principales y tu contraseña.';
}