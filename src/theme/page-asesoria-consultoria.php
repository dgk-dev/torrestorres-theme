<?php get_header(); $template_url = get_template_directory_uri();?>
<!-- ***** Breadcumb Area Start ***** -->
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo $template_url; ?>/img/bg-img/asesoria-consultoria.jpg)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcumb-content"></div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Breadcumb Area End ***** -->

<!-- ***** Regular Page Area Start ***** -->
<section class="caviar-regular-page section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="regular-page-content">
                    <div class="post-title">
                        <h1>ASESORIA Y CONSULTORIA</h1>
                    </div>
                    <div class="post-content">
                        <p>
                            Soluciones a los problemas importantes, críticos y/o complejos de las empresas.
                        </p>
                        <p>
                            Nuestra ambición es entender de raíz el problema del cliente y conformar una solución.
                        </p>
                        <p>
                            Ante la crisis, la competencia en nuestro país es férrea; la sustentabilidad de las empresas depende de muchos factores: la calidad de su administración, su gobierno corporativo su planeación estratégica debidamente aterrizada, la calidad, precisión y utilidad de su información ejecutiva para la toma de decisiones, sus acciones estratégicas, la eficacia y eficiencia de sus sistemas, el manejo de riesgos, la optimación de su carga fiscal el aprovechamiento de la economía digital, la solidez de su estructura financiera, su capacidad de competir a nivel internacional, la solidez de su recurso humano y la optimización de sus procesos.
                        </p>
                        <p>
                            Nuestros servicios de asesoría contemplan diversos y variados aspectos para asegurar el cumplimiento de soluciones a sus procesos de negocio como son información tecnológica, asesoría financiera, transacciones, fusiones escisiones y adquisiciones.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ***** Regular Page Area End ***** -->
<?php get_footer(); ?>