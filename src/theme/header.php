<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-142002784-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>TorresTorres - Asesoría y Consultoría</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/core-img/favicon.png">

        <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/core-img/favicon.png">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/core-img/favicon.png" />
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/img/core-img/apple-touch-icon-16x16.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/core-img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/core-img/apple-touch-icon-144x144.png">
    <!-- Favicons / End -->

    <!-- Core Stylesheet -->
    <!-- Responsive CSS -->
    <!-- all bundled and minified in style.css -->
    <?php wp_head(); ?>
</head>

<body>
    <?php $template_url = get_template_directory_uri(); ?>
    <!-- Preloader -->
    <div id="preloader">
        <div class="caviar-load"></div>
        <div class="preload-icons">
            <img class="preload-1" src="<?php echo $template_url; ?>/img/core-img/preload-1.png" alt="">
            <img class="preload-2" src="<?php echo $template_url; ?>/img/core-img/preload-2.png" alt="">
            <img class="preload-3" src="<?php echo $template_url; ?>/img/core-img/preload-3.png" alt="">
        </div>
    </div>

    <!-- ***** Search Form Area ***** -->
    <div class="caviar-search-form d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-close-btn" id="closeBtn">
                        <i class="pe-7s-close-circle" aria-hidden="true"></i>
                    </div>
                    <form action="#" method="get">
                        <input type="search" name="caviarSearch" id="search" placeholder="Torres Torres ...">
                        <input type="submit" class="d-none" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area" id="header">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <nav class="h-100 navbar navbar-expand-lg align-items-center">
                        <!-- Logo
                    ======================================================================== -->
                    <div calss="logo-wrapper">
                        <div class="logo">
                             <a href="<?php echo home_url(); ?>"><div class="logo-img"><img src="<?php echo $template_url; ?>/img/logo/lg-torres.png" alt="Torres-Torres" srcset="<?php echo $template_url; ?>/img/logo/lg-torres.png 1440w, <?php echo $template_url; ?>/img/logo/lg-torres-50×17.png 300w, <?php echo $template_url; ?>/img/logo/lg-torres-148×50.png 1024w" sizes="(max-width: 1440px) 100vw, 1440px"></div></a>
                        </div>  
                    </div>
                    <!-- Logo / End -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#caviarNav" aria-controls="caviarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="fa fa-bars"></span></button>
                        <div class="collapse navbar-collapse" id="caviarNav">
                            <ul class="navbar-nav ml-auto" id="caviarMenu">
                                <li class="nav-item <?php echo is_home() ? 'active' : ''; ?>">
                                    <a class="nav-link" href="<?php echo get_home_url(); ?>/#home">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo get_home_url(); ?>/#about">¿Quiénes somos?</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo get_home_url(); ?>/#menu">Nuestros Servicios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo get_home_url(); ?>/#staff">Staff</a>
                                </li>
                                <li class="nav-item <?php echo is_page('post') ? 'active' : ''; ?>">
                                    <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('post')); ?>">Post</a>
                                </li>
                                <li class="nav-item <?php echo is_page('contacto') ? 'active' : ''; ?>">
                                    <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('contacto')); ?>">Contáctanos</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->