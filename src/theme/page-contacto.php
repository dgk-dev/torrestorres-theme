<?php get_header(); $template_url = get_template_directory_uri();?>
<!-- ***** Breadcumb Area Start ***** -->
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo $template_url ?>/img/bg-img/hero-4.jpg)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcumb-content">
                    <h2>Contáctanos</h2>
                    <p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:4433401500">443 340 15 00</a> <a href="tel:4433400055">443 340 00 55</a></p>
                    <p>
                        <i class="fa fa-map-marker" aria-hidden="true"></i> 
                        Juan Sebastian Bach No. 90 
                        Col. La loma <br>
                        
                        <i class="fa" aria-hidden="true"></i> Morelia, Mich., México
                        C.P. 58290
                    </p>
                    <p><i class="fa fa-envelope-o" aria-hidden="true"></i> 
                        <a href="mailto:alvaro@torrestorres.com">alvaro@torrestorres.com</a><br>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                        <a href="mailto:primo@torrestorres.com">primo@torrestorres.com</a><br>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                        <a href="mailto:cmorales.consultor@gmail.com">cmorales.consultor@gmail.com</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Breadcumb Area End ***** -->

<!-- ***** Contact Area Start ***** -->
<div class="caviar-contact-area d-md-flex align-items-center" id="contact">
    <div class="contact-form-area d-flex justify-content-end">
        <?php echo do_shortcode('[contact-form-7 title="Contacto Torres Torres"]'); ?>
    </div>
    <div class="caviar-map-area wow fadeInRightBig" data-wow-delay="0.5s">
        <div id="googleMap"></div>
    </div>
</div>
<!-- ***** Contact Area End ***** -->
<?php get_footer(); ?>