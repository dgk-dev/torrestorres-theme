<div class="regular-page-content">
	<div class="post-title">
		<h1><?php the_title(); ?></h1>
	</div>
	<div class="post-content">
		<?php the_content(); ?>
	</div>
</div>
