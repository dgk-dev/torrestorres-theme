<?php get_header(); $template_url = get_template_directory_uri(); ?>
<!-- ***** Breadcumb Area Start ***** -->
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo $template_url; ?>/img/bg-img/hero-5.jpg)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">

            </div>
        </div>
    </div>
</div>
<!-- ***** Breadcumb Area End ***** -->
<!-- ***** Menu Area Start ***** -->
<?php 
    $args = array(
        'post_type' => 'facebook-post',
        'posts_per_page' => -1,
        // 'orderby' => 'ID',
        // 'order' => 'DESC',
        'post_status' => 'publish'
    );

    $fposts = new WP_Query($args);
?>
<div class="caviar-food-menu section-padding-150 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-10">
                <div class="caviar-menu-slides owl-carousel clearfix" >
                    <?php if($fposts->have_posts()): $count=0;?>
                        <?php while($fposts->have_posts()): $fposts->the_post(); $count++; ?>
                            <?php if($count & 1): ?>
                                <div class="caviar-portfolioss clearfix">
                                    <div style="vertical-align:top; text-align: center;">  
                            <?php endif; ?>

                                <?php the_content(); ?>
                            
                            <?php if($count%2 == 0): ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                        <?php endwhile; ?>
                            <?php if($count & 1): ?>
                                        </div>
                                    </div>
                            <?php endif; ?>
                    <?php else: ?>
                        <p>No hay posts de facebook</p>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- ***** Menu Area End ***** -->
<?php get_footer(); ?>