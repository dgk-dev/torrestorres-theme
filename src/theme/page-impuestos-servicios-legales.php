<?php get_header(); $template_url = get_template_directory_uri();?>
<!-- ***** Breadcumb Area Start ***** -->
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo $template_url; ?>/img/bg-img/impuestos-servicios-legales.jpg)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcumb-content"></div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Breadcumb Area End ***** -->

<!-- ***** Regular Page Area Start ***** -->
<section class="caviar-regular-page section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="regular-page-content">
                    <div class="post-title">
                        <h1>IMPUESTOS Y SERVICIOS LEGALES</h1>
                    </div>
                    <div class="post-content">
                        <p>
                            Nuestra práctica de Impuestos y Servicios Legales comprende un enfoque por industrias, productos y servicios especializados de eficiencia y transparencia total. Tenemos como objeto proporcionar la mejor práctica en el ámbito fiscal y legal, mediante una asesoría integral a las empresas respecto al cumplimiento de sus obligaciones tributarias y asegurándoles una situación fiscal óptima. Al mismo tiempo, les otorgamos nuestro apoyo para la solución de sus problemas de negocios.
                        </p>
                        <p>
                            Las áreas de servicio son las siguientes:
                        </p>
                        <ul>                            
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Precios de Transferencia.</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Legal Corporativo y fiscal.</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Seguro Social e Infonavit.</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Contribuciones Locales.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ***** Regular Page Area End ***** -->
<?php get_footer(); ?>