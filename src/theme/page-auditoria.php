<?php get_header(); $template_url = get_template_directory_uri();?>
<!-- ***** Breadcumb Area Start ***** -->
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo $template_url?>/img/bg-img/auditoria.jpg)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcumb-content"></div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Breadcumb Area End ***** -->

<!-- ***** Regular Page Area Start ***** -->
<section class="caviar-regular-page section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="regular-page-content">
                    <div class="post-title">
                        <h1>AUDITORIA</h1>
                    </div>
                    <div class="post-content">
                        <p>
                            Hoy en día las organizaciones y mercados exigen tener un servicio de auditoría con la máxima calidad e integridad, que cumplan rigurosamente todas las normas profesionales emitidas por los organismos reguladores nacionales e internacionales.
                        </p>
                        <p>
                            Los servicios de auditoría aportan un punto de vista independientemente acerca de las prácticas contables y los riesgos de nuestros clientes, promoviendo la transparencia en la información financiera que los diversos grupos de interés hoy demandan. 
                        </p>
                        <p>
                            Nuestros servicios de auditoría se realizan de acuerdo a nuestras políticas y procedimientos para la auditoría del control interno y de los estados financieros para efectuar una auditoría eficaz basada en la efectividad del sistema de control interno.
                        </p>
                        <h5>
                            Servicios de Auditoría:
                        </h5>
                        
                        <ul>                            
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Auditoría de estados financieros.</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Auditoría para efectos fiscales.</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Auditoria gubernamental.</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Opiniones sobre el control interno</li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Revisiones especiales como por ejemplo:
                                    <ul>
                                            <li><i class="fa fa-angle-right" aria-hidden="true"></i> Informes sobre procedimientos previamente convencidos. o Informes por procedimientos de normas para atestiguar.
                                            </li>
                                            <li><i class="fa fa-angle-right" aria-hidden="true"></i>Revisiones de carácter regulatorio.
                                            </li>
                                            <li><i class="fa fa-angle-right" aria-hidden="true"></i>Diagnósticos contables y fiscales.
                                            </li>
                                            <li><i class="fa fa-angle-right" aria-hidden="true"></i>Supervisión contable.
                                            </li>
                                            <li><i class="fa fa-angle-right" aria-hidden="true"></i>Guía en la elaboración de anexos fiscales.
                                            </li>
                                            <li><i class="fa fa-angle-right" aria-hidden="true"></i>Apoyo en la reexpresión de estados financieros. o Depuración de cuentas.
                                            </li>
                                            <li><i class="fa fa-angle-right" aria-hidden="true"></i>Arqueos de cartera.
                                            </li>
                                            <li><i class="fa fa-angle-right" aria-hidden="true"></i>Inventarios físicos de materiales y activos fijos. o Consultoría en aspectos contables.
                                            </li>
                                    </ul>
                                </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ***** Regular Page Area End ***** -->
<?php get_footer(); ?>