<?php

add_action( 'init', 'torrestorres_custom_post_type_fpost' );

function torrestorres_custom_post_type_fpost() {
	$labels = array(
		'name'               => _x( 'Posts de facebook', 'post type general name', 'text-domain' ),
		'singular_name'      => _x( 'Post de facebook', 'post type singular name', 'text-domain' ),
		'menu_name'          => _x( 'Posts de facebook', 'admin menu', 'text-domain' ),
		'add_new'            => _x( 'Añadir nuevo', 'post de facebook', 'text-domain' ),
		'add_new_item'       => __( 'Añadir nuevo post de facebook', 'text-domain' ),
		'new_item'           => __( 'Nuevo post de facebook', 'text-domain' ),
		'edit_item'          => __( 'Editar post de facebook', 'text-domain' ),
		'view_item'          => __( 'Ver post de facebook', 'text-domain' ),
		'all_items'          => __( 'Todos los posts de facebook', 'text-domain' ),
		'search_items'       => __( 'Buscar post de facebook', 'text-domain' ),
		'not_found'          => __( 'No hay post de facebook.', 'text-domain' ),
		'not_found_in_trash' => __( 'No hay post de facebook en la papelera.', 'text-domain' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Descripción.', 'torrestorrestheme' ),
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'exclude_from_search'=> true,
		'show_in_nav_menus'  => false,
		'rewrite' 			 => false,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
        'supports'           => array( 'title', 'editor' ),
        'menu_icon'          => 'dashicons-facebook',
		'menu_position'       => 4,
	);

	register_post_type( 'facebook-post', $args );
}


remove_filter('the_content','wpautop');

//decide when you want to apply the auto paragraph

add_filter('the_content','custom_formatting_fpost');

function custom_formatting_fpost($content){
    if(get_post_type()=='facebook-post') //if it does not work, you may want to pass the current post object to get_post_type
        return $content;//no autop
    else
    return wpautop($content);
}