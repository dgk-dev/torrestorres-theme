<?php

function torrestorres_resources() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	// wp_enqueue_script( 'header_js', get_template_directory_uri() . '/js/header-bundle.js', null, 1.0, false );
    
    if(is_page('contacto')){
        wp_enqueue_script( 'google-maps_js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAO0pZdljiZXwaCq-XibtJgQTNrDmAupLI', array(), '1.0', true );
    }
    wp_enqueue_script( 'footer_js', get_template_directory_uri() . '/js/footer-bundle.js', array(), '1.0', true );
    wp_localize_script('footer_js', 'footerGlobalObject', array(
        'templateUrl' => get_template_directory_uri(),
    ));
}

add_action( 'wp_enqueue_scripts', 'torrestorres_resources' );

// Contacto form 7 only in contact page

add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );
function torrestorres_cf7_resources() {
    if(is_page('contacto') && function_exists('wpcf7_enqueue_scripts') &&function_exists('wpcf7_enqueue_styles')){
        wpcf7_enqueue_scripts();
        wpcf7_enqueue_styles();
    }
}

add_action( 'wp_enqueue_scripts', 'torrestorres_cf7_resources' );


function torrestorres_login_logo() { ?>
    <style type="text/css">
        body.login{
            background: #1a1a1a;
        }

        body.login #loginform{
            background: #212121;
            color: #f1f1f1;
        }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_template_directory_uri(); ?>/img/logo/lg-torres-148×50.png);
            height:99px;
            width:148px;
            background-size: 148px 99px;
            background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'torrestorres_login_logo' );

add_filter( 'login_headerurl', 'torrestorres_loginlogo_url');

function torrestorres_loginlogo_url($url) {
    return home_url();
}