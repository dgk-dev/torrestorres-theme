<?php get_header(); $template_url = get_template_directory_uri();?>
<!-- ****** Welcome Area Start ****** -->
<section class="caviar-hero-area" id="home">
	<!-- Welcome Social Info -->
	<div class="welcome-social-info">
		<a href="https://www.facebook.com/consultoresTTM" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		<a href="#"><i class="fa fa-instagram visually-hidden" aria-hidden="true"></i></a>
		<a href="#"><i class="fa fa-twitter visually-hidden" aria-hidden="true"></i></a>
	</div>
	<!-- Welcome Slides -->
	<div class="caviar-hero-slides owl-carousel">
		<!-- Single Slides -->
		<div class="single-hero-slides bg-img" style="background-image: url(<?php echo $template_url ?>/img/bg-img/hero-1.jpg);">
			<div class="container h-100">
				<div class="row h-100 align-items-center">
					<div class="col-11 col-md-6 col-lg-4">
						<div class="hero-content">
							<h2>Torres Torres</h2>
							<p>Somos un despacho con más de 20 años de experiencia que ofrece servicios de Auditoría, asesoría y consultoría, así como de contabilidad a empresas de diversos ramos.</p>
						</div>
					</div>
				</div>
			</div>
			<!-- Slider Nav -->
			<div class="hero-slides-nav bg-img" style="background-image: url(<?php echo $template_url ?>/img/bg-img/hero-2.jpg);"></div>
		</div>
		<!-- Single Slides -->
		<div class="single-hero-slides bg-img" style="background-image: url(<?php echo $template_url ?>/img/bg-img/hero-2.jpg);">
			<div class="container h-100">
				<div class="row h-100 align-items-center">
					<div class="col-11 col-md-6 col-lg-4">
						<div class="hero-content">
							<h2>SAT</h2>
							<p>Te recomendamos estar preparado el 1 de septiembre para emitir este tipo de comprobantes y evitar sanciones por parte de la autoridad fiscal.</p>
							<a href="#" class="btn caviar-btn"><span></span> VER DETALLE</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Slider Nav -->
			<div class="hero-slides-nav bg-img" style="background-image: url(<?php echo $template_url ?>/img/bg-img/hero-1.jpg);"></div>
		</div>
	</div>
</section>
<!-- ****** Welcome Area End ****** -->

<!-- ****** About Us Area Start ****** -->
<section class="caviar-about-us-area section-padding-150" id="about">
	<div class="container">
		<!-- About Us Single Area -->
		<div class="row align-items-center">
			<div class="col-12 col-md-6">
				<div class="about-us-thumbnail wow fadeInUp" data-wow-delay="0.5s">
					<img src="<?php echo $template_url ?>/img/bg-img/about-1.jpg" alt="">
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-5 ml-md-auto">
				<div class="section-heading">
					<h2>¿Quiénes somos?</h2>
				</div>
				<div class="about-us-content">
					<span>TORRES TORRES Y CíA S.C.</span>
					<p>
						Es un despacho con más de 20 años de experiencia que ofrece servicios de Auditoría, asesoría y consultoría, así como de contabilidad a empresas de diversos ramos, inició de operaciones en el año 1991 con el nombre de Torres Gómez y Cía S.C., posteriormente con el de TG Auditores Independientes, S.C., Ubicado actualmente en Juan Sebastián Bach., No. 90 Col. La loma C.P. 58290 en Morelia, Michoacán.
						Somos un despacho que cuenta con los conocimientos y la experiencia con el firme propósito de ofrecer soluciones y servicios integrales que llevan a nuestros clientes a la innovación y mejora continua siempre basándonos en la razón y pasión por nuestro trabajo.
					</p>

				</div>
				<a href="<?php echo get_permalink(get_page_by_path('quienes-somos')); ?>" class="btn caviar-btn"><span></span> Ver detalle</a>
			</div>
		</div>

<!-- About Us Single Area -->
	</div>
</section>
<!-- ****** About Us Area End ****** -->

<!-- ****** Dish Menu Area Start ****** -->
<section class="caviar-dish-menu" id="menu">
	<div class="container">
		<div class="row">
			<div class="col-12 menu-heading">
				<div class="section-heading text-center">
					<h2>Nuestros Servicios</h2>
				</div>
				<!-- btn -->
				
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-6 col-md-4">
				<div class="caviar-single-dish wow fadeInUp" data-wow-delay="0.5s">
					<img src="<?php echo $template_url ?>/img/menu-img/dish-1.png" alt="">
					
					<div class="dish-info">
						<h6 class="dish-name">AUDITORIA</h6>
						<a href="<?php echo get_permalink(get_page_by_path('auditoria')); ?>" class="dish-price"><span></span>Ver</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4">
				<div class="caviar-single-dish wow fadeInUp" data-wow-delay="1s">
					<img src="<?php echo $template_url ?>/img/menu-img/dish-2.png" alt="">
					<div class="dish-info">
						<h6 class="dish-name">ASESORIA Y CONSULTORIA</h6>
							<a href="<?php echo get_permalink(get_page_by_path('asesoria-consultoria')); ?>" class="dish-price"><span></span>Ver</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-4">
				<div class="caviar-single-dish wow fadeInUp" data-wow-delay="1.5s">
					<img src="<?php echo $template_url ?>/img/menu-img/dish-3.png" alt="">
					<div class="dish-info">
						<h6 class="dish-name">IMPUESTOS Y SERVICIOS LEGALES</h6>
							<a href="<?php echo get_permalink(get_page_by_path('impuestos-servicios-legales')); ?>" class="dish-price"><span></span>Ver</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ****** Dish Menu Area End ****** -->

<!-- ****** Testimonials Area Start ****** -->
<section class="caviar-testimonials-area" id="staff">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="testimonials-content">
					<div class="section-heading text-center">
						<h2>Staff</h2>
					</div>
					<div class="caviar-testimonials-slides owl-carousel">
						<!-- Single Testimonial Area -->
						<div class="single-testimonial">
							<div class="testimonial-thumb-name d-flex align-items-center">
								<img src="<?php echo $template_url ?>/img/testimonial-img/3.png" alt="">
								<div class="tes-name">
									<h5>Álvaro Torres Arenal</h5>
									<p>C.P.C.</p>
								</div>
							</div>
							<p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Contabilidad Financiera y Gubernamental</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Asesoría Financiera
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Auditoria Financiera, Administrativa y Operacional
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Transferencia, Defensa Fiscal y Recuperación de Impuestos
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Consultor en Planeación Estrategica para el Impulso y Desarrollo de las
								Empresas
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Consultor en Asuntos Corporativos
								</p>
							</p>
						</div>
						<!-- Single Testimonial Area -->
						<div class="single-testimonial">
							<div class="testimonial-thumb-name d-flex align-items-center">
								<img src="<?php echo $template_url ?>/img/testimonial-img/2.png" alt="">
								<div class="tes-name">
									<h5>Carlos Primo Torres Gómez</h5>
									<p>C.P.C.</p>
								</div>
							</div>
							<p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Contabilidad Financiera y Gubernamental</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Asesoría Financiera
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Auditoria Financiera, Administrativa y Operacional
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Transferencia, Defensa Fiscal y Recuperación de Impuestos
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Consultor en Planeación Estrategica para el Impulso y Desarrollo de las
								Empresas
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Consultor en Asuntos Corporativos
								</p>
							</p>
						</div>
						<!-- Single Testimonial Area -->
						<div class="single-testimonial">
							<div class="testimonial-thumb-name d-flex align-items-center">
								<img src="<?php echo $template_url ?>/img/testimonial-img/1.png" alt="">
								<div class="tes-name">
									<h5>Carlos David Morales Pérez</h5>
									<p>L.C. y M.A.</p>
								</div>
							</div>
							<p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Contabilidad Financiera y Gubernamental</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Asesoría Financiera
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Auditoria Financiera, Administrativa y Operacional
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Transferencia, Defensa Fiscal y Recuperación de Impuestos
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Consultor en Planeación Estrategica para el Impulso y Desarrollo de las
								Empresas
								</p>
								<p><i class="fa fa-diamond" aria-hidden="true"></i>
								Consultor en Asuntos Corporativos
								</p>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ****** Testimonials Area End ****** -->
<?php get_footer(); ?>