<?php get_header(); $template_url = get_template_directory_uri();?>
<!-- ***** Breadcumb Area Start ***** -->
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo $template_url ?>/img/bg-img/about-1.jpg)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcumb-content"></div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Breadcumb Area End ***** -->

<!-- ***** Regular Page Area Start ***** -->
<section class="caviar-regular-page section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="regular-page-content">
                    <div class="post-title">
                        <h1>NUESTRA VISION</h1>
                    </div>
                    <div class="post-content">
                        <p>
                            <strong>TORRES TORRES Y CÍA S.C.</strong> es una firma especializada en los servicios de <strong>asesoría, consultoría, contabilidad y auditoría fiscal y financiera.</strong> Su área de acción abarca la planeación y estructuración de operaciones; la consolidación para fines fiscales; los estudios para la elaboración de solicitudes y su trámite para la dictaminación de resoluciones fiscales; jurÍdico en materia administrativa fiscal; y elaboración de dictámenes de estados financieros tanto para efectos fiscales como financieros.</p>
                        <p>
                            Dedicada a atender las necesidades de todo tipo de organizaciones, <strong>TORRES TORRES Y CÍA S.C.</strong> tiene un amplio reconocimiento en el medio empresarial en el estado de Michoacán y el Bajío.
                        </p>
                    </div>

                    <div class="post-title">
                        <h2>NUESTROS VALORES</h2>
                    </div>
                    <div class="post-content">
                        <p>
                            Honestidad, disponibilidad, profesionalismo, interés por los clientes y responsabilidad social.
                        </p>
                        <p>
                            Dedicada a atender las necesidades de todo tipo de organizaciones, <strong>TORRES TORRES Y CÍA S.C.</strong> tiene un amplio reconocimiento en el medio empresarial en el estado de Michoacán y el Bajío.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Regular Page Area End ***** -->
<?php get_footer(); ?>