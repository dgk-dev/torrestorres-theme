<?php get_header(); $template_url = get_template_directory_uri();?>
<!-- ***** Breadcumb Area Start ***** -->
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo $template_url ?>/img/bg-img/about-1.jpg)">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcumb-content"></div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Breadcumb Area End ***** -->

<!-- ***** Regular Page Area Start ***** -->
<section class="caviar-regular-page section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="regular-page-content error-404 not-found">
                    <div class="post-title">
                        <h1>404</h1>
                    </div>
                    <div class="post-content">
						<p>Parece que la página que buscas no está en el servidor</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Regular Page Area End ***** -->
<?php get_footer(); ?>